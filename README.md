# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Laravel + Axios test work
https://www.youtube.com/watch?v=6AU-tkobg4c&feature=youtu.be

### How do I get set up? ###

In Terminal (Linux, MacOS) or CMD (Windows) run next command: 

**git clone https://grimplenet@bitbucket.org/grimplenet/laravel-axios.git**

After this run next command in your folder: 

**cd laravel-axios**

After this run next command: 

**npm i**

After this you can running server by next command: 

**php artisan serve**